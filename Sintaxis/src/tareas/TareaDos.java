/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareas;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class TareaDos {

    public static void main(String[] args) {
        String menu = "\nTarea Dos - v0.1\n"
                + "1. Digite un número\n"
                + "2. Primo/Compuesto\n"
                + "3. Tipo Número\n"
                + "4. Salir\n"
                + "Digite una opción";

        OtroNumero tarea = null;

        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    int num = Util.leerInt("Digite un número");
                    if (tarea == null) {
                        tarea = new OtroNumero(num);
                    } else {
                        tarea.setNumero(num);
                    }
                    break;
                case 2:
                    if (tarea != null) {
                        System.out.print("El número " + tarea.getNumero());
                        System.out.println(tarea.esPrimo()
                                ? " es primo"
                                : " es compuesto");
                    } else {
                        System.out.println("Favor digite un número");
                    }
                    break;
                case 3:
                    if (tarea != null) {
                        int tipo = tarea.tipoNumero();
                        System.out.print("El número " + tarea.getNumero());
                        switch (tipo) {
                            case 1:
                                System.out.println(" es Perfecto");
                                break;
                            case 2:
                                System.out.println(" es Deficiente");
                                break;
                            default:
                                System.out.println(" es Abundante");
                        }
                    } else {
                        System.out.println("Favor digite un número");
                    }
                    break;
                case 4:
                    System.out.println("Gracias por utilizar la aplicación");
                    break APP;
                default:
                    System.out.println("Opción Inválida!!");
            }
        } while (true);

    }
}
