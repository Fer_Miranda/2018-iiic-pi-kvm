/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static void main(String[] args) {
        Juego j = new Juego();
        while (true) {
            int num = Util.leerInt("#");
            j.jugar(num);
            if (j.isGano()) {
                System.out.println("Ganó");
                break;
            } else if (j.perdio()) {
                System.out.println("Perdió, se acabaron los intentos");
                break;
            } else {
                System.out.println(j.pista(num));
            }
        }

    }
}
