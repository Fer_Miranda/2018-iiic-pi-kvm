/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import java.util.Scanner;
import util.Util;

/**
 *
 * @author ALLAN
 */
public class EstructuraIterativa {

    public static void main(String[] args) {
        int tabla = Util.leerInt("Tabla");
//
//        int con = 1;
//        while (con <= 10) {
//            String res = String.format("%dx%d=%d", tabla, con, (tabla * con));
//            System.out.println(res);
//            //System.out.println(tabla + "x" + con + "=" + (tabla * con));
//            con++;
//        }
//
//        con = 1;
//        int sum = 0;
//        String tex = "";
//        while (con <= 10) {
//            int num = Util.leerInt("#" + con);
//            if (num < 0) {
//                break;
//            }
//            sum += num;
//            tex += (con == 1 ? "" : "+") + num;
//
//            con++;
//        }
//        System.out.println(tex + "=" + sum);

//        int con = 1;
//        do {
//            System.out.println("Ejecutar-Repetir");
//            System.out.println(con);
//            con++;
//        } while (con < 1);
//        
//        con = 1;
//        while (con <= 10) {
//            System.out.println("Ejecutar-Repetir");
//            System.out.println(con);
//            con++;
//        }
        for (int i = 1; i <= 10; i++) {
            String res = String.format("%dx%d=%d", tabla, i, (tabla * i));
            System.out.println(res);
        }
        String res = ""; 
        for (int i = 1; i <= 1000; i++) {
            if (i % 7 == 0) {
                res += i + "\n";
            }
        }
        System.out.println(res);

    }

}
