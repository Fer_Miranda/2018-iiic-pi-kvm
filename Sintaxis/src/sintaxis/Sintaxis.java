/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import java.math.MathContext;
import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class Sintaxis {

    static int numId;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        int id = 0;
//        System.out.println(id);
//        System.out.println("Hola Mundo!!");
//        
//        //Tipos de Variables 
//        boolean bandera = true;
//        char letra = 'a';
//        char digito = '9';
//        char simbolo = '#';
//        
//        //Secuencias de escape
//        char escape = '\n';
//        System.out.println("Hola\nMundo");
//        System.out.println("Hola\tMundo");
//        System.out.println("Hola\\Mundo");
//        System.out.println("Hola \"Mundo\"");
//        System.out.println("\u221A = 123");
//        System.out.println("\u0470 = 123");
//        
//        //Tipos númericos por defecto
//        int numero = 123;
//        double decimales = 12.23;
//        
//        //Cadenas de texto o String
//        String texto = "Hola\tMundo";
//        System.out.println("A\tB\tC\n1\t2\t3\n");
//        System.out.println("\"Hola\"");
//        System.out.println("El caracter de escape \\n cambia de línea");
//        System.out.println("El UNICODE \\u0040 vale \u0040");
//        System.out.println("El UNICODE \\u00D1 vale \u00D1");
//
//        //Variables 
//        //Declaración de una variable 
//        int numEstudiantes;
//        //Inicialización de una variable 
//        numEstudiantes = 17;
//        System.out.println(numEstudiantes);
//        //Declarar e inicializar variable en una sola línea
//        String texto = "Hola Mundo";
//        //Asignación
//        numEstudiantes = 18;
//
//        final String RUTA = "C:\\Proyectos\\UTN\\Robotica";
//        final int MAX_EST = 20;
//
////        int a;
////        int b = 0;
////        int c = 10;
////        a = b + c;
//        boolean isReal = true;
//        byte b = 122;
//        short s = -29000;
//        int i = 100000;
//        long l = 999999999999L;
//        float f = 234.99F;
//        double d;
//        char cvalue = '4';
//        final double PI = 3.1415926;
//
//        System.out.println((int) 32.2999);
//        System.out.println((double) 32);
//        System.out.println((byte) 120);
//
//        //Operadores aritmeticos  y unarios
//        int cont = 0;
//        System.out.println(++cont);
//        System.out.println(cont++);
//
//        System.out.println("Primera Linea");
//        System.out.println("Segunda Linea");
//        System.out.println("Tercera Linea");
//
//        String nombre = JOptionPane.showInputDialog("Digite su nombre: ");
//
//        JOptionPane.showMessageDialog(null, nombre, "Nombre",
//                JOptionPane.PLAIN_MESSAGE);
//        JOptionPane.showMessageDialog(null, nombre, "Nombre",
//                JOptionPane.WARNING_MESSAGE);
//
//        JOptionPane.showMessageDialog(null, nombre, "Nombre",
//                JOptionPane.QUESTION_MESSAGE);
//
//        JOptionPane.showMessageDialog(null, nombre, "Nombre",
//                JOptionPane.ERROR_MESSAGE);
        Logica log = new Logica();
        int x = 10;
        int y = 15;
        System.out.println(Logica.sumar(x, y));
        System.out.println(log.restar(x, y));
        System.out.println(log.multiplicar(x, y));
        System.out.println(log.dividir(x, y)); 

    }
}
