/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

/**
 *
 * @author ALLAN
 */
public class ExpresionesBooleanas {

    public static void main(String[] args) {
        System.out.println("Expresiones Booleanas");
        System.out.println("5==5 : " + (5 == 5));
        System.out.println("5==6 : " + (5 == 6));
        System.out.println("5!=6 : " + (5 != 6));
        System.out.println("5 >6 : " + (5 > 6));
        System.out.println("5<=6 : " + (5 <= 6));
        System.out.println("6<=6 : " + (6 <= 6));

        //Imprimir la tabla de verdad del AND - &&
        //true && true = true
        System.out.println("\nAND(&&)");
        System.out.println("true  && true  = " + (true && true));
        System.out.println("true  && false = " + (true && false));
        System.out.println("false && true  = " + (false && true));
        System.out.println("false && false = " + (false && false));

        //Imprimir la tabla de verdad del OR - ||
        //true || true = true
        System.out.println("\nOR(||)");
        System.out.println("true  || true  = " + (true || true));
        System.out.println("true  || false = " + (true || false));
        System.out.println("false || true  = " + (false || true));
        System.out.println("false || false = " + (false || false));

        System.out.println("\nNOT(!)");
        System.out.println("!true  = " + !true);
        System.out.println("!false = " + !false);
    }
}






