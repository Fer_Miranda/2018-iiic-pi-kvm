/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Gasolinera {

    private double precioLitro;
    private double galones;
    private final double LITROS_X_GAL;

    public Gasolinera() {
        precioLitro = 730;
        LITROS_X_GAL = 3.78541;
    }

    public double calcularCobro() {
        return galones * LITROS_X_GAL * precioLitro;
    }

    public double getPrecio() {
        return precioLitro;
    }

    public void setPrecio(double precioLitro) {
        this.precioLitro = precioLitro;
    }

    public double getGalones() {
        return galones;
    }

    public void setGalones(double galones) {
        this.galones = galones;
    }

}
