/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Circunferencia {

    private double radio;

    public Circunferencia() {
    }

    
    
    public Circunferencia(double radio) {
        this.radio = radio;
    }

    /**
     * Calcula el área de una circunferencia
     *
     * @return double area calculada
     */
    public double calcularArea() {
        return Math.PI * Math.pow(radio, 2);
    }

    /**
     * Calcula el perímetro de una circunferencia
     *
     * @return double area calculada
     */
    public double calcularPerimetro() {
        return 2 * Math.PI * radio;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
}
