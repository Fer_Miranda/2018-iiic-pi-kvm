/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Temperatura {

    private double gradosCentigrados;

    public Temperatura(double gradosCentigrados) {
        this.gradosCentigrados = gradosCentigrados;
    }

    /**
     * Convierte los grados centigrados  a Farenheit
     * @return double con los grados farenheit
     */
    public double convertirAFarenheit() {
        return gradosCentigrados * 9 / 5 + 32;
    }

    public double getGradosCentigrados() {
        return gradosCentigrados;
    }

    public void setGradosCentigrados(double gradosCentigrados) {
        this.gradosCentigrados = gradosCentigrados;
    }

}
