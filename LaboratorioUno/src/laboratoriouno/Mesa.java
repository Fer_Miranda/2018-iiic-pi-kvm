/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Mesa {

    private int cHamSec;
    private int cHamQue;
    private int cHamEsp;
    private int cPapas;
    private int cRefresco;
    private int CPostre;

    private final int pHamSec;
    private final int pHamQue;
    private final int pHamEsp;
    private final int pPapas;
    private final int pRefresco;
    private final int pPostre;

    public Mesa() {
        this.pHamSec = 15;
        this.pHamQue = 18;
        this.pHamEsp = 20;
        this.pPapas = 8;
        this.pRefresco = 5;
        this.pPostre = 6;
    }

    public void capturarOrden(int pro, int can) {
        switch (pro) {
            case 1:
                cHamSec += can;
                break;
            case 2:
                cHamQue += can;
                break;
            case 3:
                cHamEsp += can;
                break;
            case 4:
                cPapas += can;
                break;
            case 5:
                cRefresco += can;
                break;
            case 6:
                CPostre += can;
                break;

        }
    }

    public double calcularCuenta() {
        double total = cHamSec * pHamSec
                + cHamQue * pHamQue
                + cHamEsp * pHamEsp
                + cPapas * pPapas
                + cRefresco * pRefresco
                + CPostre * pPostre;
        return total;
    }

    public String menu() {
        String str = String.format("Menu - v.01\n"
                + "1. Hamburguesa Sencilla(%d)\n"
                + "2. Hamburguesa con Queso(%d)\n"
                + "3. Hamburguesa Especial(%d)\n"
                + "4. Papas Fritas(%d)\n"
                + "5. Refresco (%d)\n"
                + "6. Postre(%d)\n", pHamSec, pHamQue, pHamEsp, pPapas,
                pRefresco, pPostre);
        return str;
    }

}
