/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Fecha {

    private int dia;
    private int mes;
    private int anno;

    public Fecha() {
        dia = 1;
        mes = 1;
        anno = 2000;
    }

    public Fecha(int dia, int mes, int anno) {
        if (verificar(dia, mes, anno)) {
            this.dia = dia;
            this.mes = mes;
            this.anno = anno;
        } else {
            this.dia = 1;
            this.mes = 1;
            this.anno = 2000;
        }
    }

    public void setFecha(int dia, int mes, int anno) {
        if (verificar(dia, mes, anno)) {
            this.dia = dia;
            this.mes = mes;
            this.anno = anno;
        } else {
            this.dia = 1;
            this.mes = 1;
            this.anno = 2000;
        }
    }

    public String getFecha() {
        String r = String.format("%02d/%02d/%04d", dia, mes, anno);
        return r;
    }

    public String getFechaLetras() {
        String r = String.format("%d de %s de %d", dia, convertir(mes), anno);
        return r;
    }

    private String convertir(int mes) {
        switch (mes) {
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            default:
                return "Diciembre";
        }
    }

    public boolean verificar(int dia, int mes, int anno) {

        if (dia < 1 || dia > 31 || mes < 1 || mes > 12) {
            return false;
        }

        if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia > 30) {
            return false;
        }

        if (mes == 2 && !esBisiesto(anno) && dia > 28) {
            return false;
        }

        if (mes == 2 && dia > 29) {
            return false;
        }

        return true;
    }

    private boolean esBisiesto(int anno) {
        if (anno % 4 == 0) {
            if (anno % 100 == 0) {
                return anno % 400 == 0;
            }
            return true;
        }
        return false;
    }

}
